package ru.tsc.apozdnov.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationWebSpringBoot {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationWebSpringBoot.class, args);
    }
}
